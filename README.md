# Introduction
The InfernOS Graphics Foundation is a project which will become the (well..) foundation of the graphics in the upcoming InfernOS.
Ain't much else to tell (for now), enjoy your stay.

# Getting Started
Basically it is a VS project

1.	Clone / Download / Telegrab / Whatever the source.

2.	You need to have the following libraries in order for the program to build correctly:

 * *GLFW*
 * *GLM*
 * *glad (bundled)*
 * *stbi_image*

3. Link the libraries to the project and build.

4. Links to dependencies:

  * [GLFW](http://www.glfw.org/)
  * [GLM](http://glm.g-truc.net/0.9.8/index.html)

# Build and Test
Build with VS2017 while correctly linking the following libraries specified above.
Testing:

 * Break stuff and report.
 * Write unit tests.
 * Examine the code.
 * Suggest improvements.

# FAQ
### What is the InfernOS project?
It's a (hypothetical) Linux-based OS, that's all you need to know for now.

### Is the OS ready yet?
No.

### When will the OS be released?
Who knows? Maybe tomorrow, maybe 10 years from now.

### How I got here?
You're asking me?

### I want to help
Sure, either build and test (see aforementioned) or
contact me and I'll fill you in with the details if you
want to get serious.


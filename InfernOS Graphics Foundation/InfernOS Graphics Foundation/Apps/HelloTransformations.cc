#include "../Dependencies/glad/glad.h"
#include "../Dependencies/GLFW/glfw3.h"
#include "../Dependencies/glm/glm.hpp"
#include "../Dependencies/glm/gtc/matrix_transform.hpp"
#include "../Dependencies/glm/gtc/type_ptr.hpp"
#include "../Core/MonitorManager.h"
#include <iostream>
#include "apps.h"

using namespace HelloTransformations;

GLvoid handleInput(GLFWwindow*);

const GLchar* HelloTransformations::vertexShaderSource = \
R"(#version 460 core
layout (location = 0 ) vec3 posA;
layout (location = 1 ) vec2 texCoordA;

out vec2 texCoord;

void main()
{
	gl_Position = vec4(posA, 1);
	texCoord = texCoordA;
}
)";

const GLchar* HelloTransformations::fragmentShaderSource = \
R"(#version 460 core
in vec2 texCord;

out vec4 color;
void main()
{
	color = vec4(1, 1, 1, 1);
}
)";

GLint HelloTransformations::appMain()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	MonitorManager mm;
	mm.setActiveMonitor(0);

	
	glfwTerminate();
	return 0;
}
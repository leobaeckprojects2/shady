// MIT License
//
// Copyright(c) 2018 Elian (Shadi) Kamal
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "../Dependencies/glad/glad.h"
#include "../Dependencies/GLFW/glfw3.h"
#include "MonitorManager.h"

MonitorManager::MonitorManager()
{
	this->updateMonitors();
	if (this->m_monitorCount > 0)
		this->m_activeMonitor = glfwGetPrimaryMonitor();
}

MonitorManager::MonitorManager (GLFWmonitorfun cb)
{
	this->updateMonitors();
	if (this->m_monitorCount > 0)
		this->m_activeMonitor = glfwGetPrimaryMonitor();
	glfwSetMonitorCallback(cb);
}

MonitorManager::~MonitorManager() {}

GLvoid MonitorManager::updateMonitors() { this->m_monitors = glfwGetMonitors(&this->m_monitorCount); }

const GLint MonitorManager::getMonitorCount() { return this->m_monitorCount; }

const GLFWvidmode* MonitorManager::getActiveMonitorVidMode() { return glfwGetVideoMode(this->m_activeMonitor); }

GLint MonitorManager::getActiveMonitorWidth() { return this->MonitorManager::getActiveMonitorVidMode()->width; }

GLint MonitorManager::getActiveMonitorHeight() { return this->MonitorManager::getActiveMonitorVidMode()->height; }

GLFWmonitor* MonitorManager::getActiveMonitor() { return this->m_activeMonitor; }

GLvoid MonitorManager::setActiveMonitor(GLint n)
{
	this->updateMonitors();
	GLint count = this->getMonitorCount();
	if (n < 0 || n >= count) return;
	else this->m_activeMonitor = this->m_monitors[n];
}

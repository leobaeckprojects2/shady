// MIT License
//
// Copyright(c) 2018 Elian (Shadi) Kamal
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once
#include "../Dependencies/glad/glad.h"
#include "../Dependencies/GLFW/glfw3.h"

// Implements an abstraction layer for controlling multiple attached monitors
// Example:
// MonitorManager mm;
// if (mm.getMonitorCount() > 1) mm.setActiveMonitor(1);
// else mm.setActiveMonitor(0);
// GLFWwindow* window = glfwCreateWindow(mm.getActiveMonitorWidth(),
//	 mm.getActiveMonitorHeight(), TITLE,
//	 mm.getActiveMonitor(), nullptr);
// glfwMakeContextCurrent(window);
class MonitorManager
{
public:
	MonitorManager();
	MonitorManager(GLFWmonitorfun);
	~MonitorManager();
	GLFWmonitor* getActiveMonitor();
	const GLint getMonitorCount();
	const GLFWvidmode* getActiveMonitorVidMode();
	GLint getActiveMonitorWidth();
	GLint getActiveMonitorHeight();
	GLvoid setActiveMonitor(GLint);
private:
	// Updates attached monitors status.
	// Monitor list memory is handled and freed by GLFW,
	// No additional memory is allocated.
	// No need to invoke upon object creation as the constructor already does.
	// Function should be called only in the event of
	// peripherial changes detection (i.e new monitors attached/removed/destroyed).
	// Peripherial changes detection and handling are up to the user.
	GLvoid updateMonitors();
	GLint m_monitorCount;
	GLFWmonitor* m_activeMonitor;
	GLFWmonitor** m_monitors;
};

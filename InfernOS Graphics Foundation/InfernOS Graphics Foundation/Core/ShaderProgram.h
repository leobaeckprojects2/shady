// MIT License
//
// Copyright(c) 2018 Elian (Shadi) Kamal
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once
#include "../Dependencies/glad/glad.h"
#include <string>

namespace ShaderProgramErrors
{

	extern const GLchar* ERR_SHADER_VERTEX_FILE_PATH_INVALID;
	extern const GLchar* ERR_SHADER_FRAGMENT_FILE_PATH_INVALID;
	extern const GLchar* ERR_SHADER_VERTEX_COMPILATION_FAILURE;
	extern const GLchar* ERR_SHADER_FRAGMENT_COMPILATION_FAILURE;
	extern const GLchar* ERR_SHADER_PROGRAM_LINKING_FAILURE;
	extern const GLchar* ERR_VALIDATE_PCOMPONENT_INVALID;
	extern const GLchar* ERR_VALIDATE_SCOMPONENT_INVALID;
	extern const GLchar* ERR_VALIDATE_COMPONENT_SELECTION;
	extern const GLchar* ERR_LOAD_UNIFORM_TYPE_SELECTION;
	extern const GLchar* ERR_LOAD_UNIFORM_SIZE_SELECTION;
	extern const GLint INFO_LOG_BUF_SIZE;
}

// OpenGL shader program wrapper with GLSL shader file loading
// capabilities (w/ Vetex and Fragment shaders only).
// Supports loading of scalar, vector, and matrix uniforms.
// Example:
// ShaderProgram sp("foo/bar/VertexShaderSource.glsl", "baz/FragmentShaderSource.glsl");
// sp.process();
// sp.loadUniform("mod", FLOAT, 3, 0.2f, 1.8f, 0.44f);
// sp.loadUniformMatrix("matmod", DOUBLE_M, 2, 3, is_transposed, matmodobj*);
class ShaderProgram
{
public:
	enum UniformType : GLushort {
		DOUBLE, 
		DOUBLE_V,
		DOUBLE_M,
		FLOAT,
		FLOAT_V,
		FLOAT_M,
		INT,
		INT_V,
		UINT,
		UINT_V
	};
	ShaderProgram(const GLchar*, const GLchar*);
	~ShaderProgram();
	GLvoid process();
	GLvoid useProgram();
	template <typename T> GLvoid loadUniform(const GLchar*, UniformType, GLuint, T, T, T, T);
	template <typename U> GLvoid loadUniformVector(const GLchar*, UniformType, GLuint, U);
	template <typename M> GLvoid loadUniformMatrix(const GLchar*, UniformType, GLuint, GLuint, GLboolean, M);
	
private:
	enum Component {
		SHADER = 10,
		PROGRAM = 20,
		VERTEX = 100,
		FRAGMENT = 200,
		NONE = 0
	};
	GLuint _ID;
	std::string _vertexShaderData;
	std::string _fragmentShaderData;
	GLvoid validate(GLuint, Component, Component);
};